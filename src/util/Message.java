package util;

public class Message {
    private int idMessage; 
    private String subject; 
    private String priority; 
	private String message;
    private String from;
    private String to;
    private boolean isNew;
    private String time;

    public Message(int idMessage, String subject, String priority, String message, String from, String to,
			boolean isNew, String time) {
		this.idMessage = idMessage;
		this.subject = subject;
		this.priority = priority;
		this.message = message;
		this.from = from;
		this.to = to;
		this.isNew = isNew;
		this.time = time;
	}

	public String getTo(){
		return to;
    }

    public String getFrom(){
    	return from;
    }

    public String getMessage(){
    	return message;
    }

	public int getIdMessage() {
		return idMessage;
	}

	public String getSubject() {
		return subject;
	}

	public String getPriority() {
		return priority;
	}

	public boolean isNew() {
		return isNew;
	}
	
	public String getTime() {
		return time;
	}
}
