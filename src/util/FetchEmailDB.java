
package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class FetchEmailDB {

	private final String url = "jdbc:postgresql://localhost/WS";
    private final String user = "postgres";
    private Connection conn; 
	
    public void getDatabaseConnection() {
        try {
            this.conn = DriverManager.getConnection(url, user, "");
            System.out.println("Successfully connected to the database!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean sendMessage(String from, String to, String subject, String content, String priority) {
    	String query = "INSERT INTO service_message VALUES (DEFAULT, ?, ?, ?, ?, DEFAULT, ?::public.priority, DEFAULT)"; 
    	try(PreparedStatement pstmt = this.conn.prepareStatement(query)){
    		pstmt.setInt(1, getUserId(from));
    		pstmt.setInt(2, getUserId(to));
    		pstmt.setString(3, subject);
    		pstmt.setString(4, content);
    		pstmt.setString(5, priority);
    		pstmt.executeUpdate();
    	} catch(SQLException e) {
    		e.printStackTrace();
    		return false;
    	}    	
    	return true;
    }

    public ArrayList<Message>getAllMessagesForUser(String user) {
    	ArrayList<Message>resultSet = new ArrayList<>(); 
    	String query = "SELECT * FROM service_message WHERE recipient = ?";
    	
    	try(PreparedStatement pstmt = this.conn.prepareStatement(query)){
    		pstmt.setInt(1, getUserId(user));
    		ResultSet rs = pstmt.executeQuery();
    		while(rs.next()) {
    			resultSet.add(buildMessage(rs.getInt("id_msg"),getUsernameFromId(rs.getInt("author")), rs.getString("subject"),
    					rs.getString("content"), ! rs.getBoolean("seen"), rs.getString("message_priority"), user, rs.getTimestamp("time"))); 
    		}
    		
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}
    	
    	return resultSet; 
    }
   
    public void makeMeassageSeen(int idMessage) {
    	String query = "UPDATE service_message SET seen=true WHERE id_msg = ?"; 
    	try(PreparedStatement pstmt = this.conn.prepareStatement(query)){
    		pstmt.setInt(1, idMessage);
    		pstmt.executeUpdate();
    	
    		
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}    
    }
 
    public boolean removeMessage(int idMessage) {
    	String query = "DELETE FROM service_message WHERE id_msg = ?"; 
    	try(PreparedStatement pstmt = this.conn.prepareStatement(query)){
    		pstmt.setInt(1, idMessage);
    		pstmt.executeUpdate();
    		return true; 
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}    	
    	return false;
    }

    public boolean removeMailsContaining(String user) {
    	boolean res = false; 
    	String query = "DELETE FROM service_message WHERE recipient = ? and subject LIKE '%SPAM%'"; 
    	try(PreparedStatement pstmt = this.conn.prepareStatement(query)){
    		pstmt.setInt(1, getUserId(user));
    		pstmt.executeUpdate();
    		res = true; 
    	}catch(SQLException e) {
    		e.printStackTrace();
    	}    	
    	return res; 
    }
    private Message buildMessage(int id,String author, String subject, String content, boolean read, String priority, String recipient, Timestamp time) {
    	return new Message(id, subject, priority, content, author, recipient, read, time.toString()); 
    }

   private int getUserId(String username) {
	   int result = -1; 
	   String query = "SELECT id_user FROM service_user WHERE username = ? ";
	   try(PreparedStatement pstmt = this.conn.prepareStatement(query)) {
		   pstmt.setString(1, username);
		   ResultSet rs = pstmt.executeQuery();
		   if(rs.next()) {
			   result = rs.getInt("id_user"); 
		   }
	   }catch(SQLException e) {
		   e.printStackTrace();
	   }
	   return result; 
   }

   private String getUsernameFromId(int id) {
	   String result = null; 
	   String query = "SELECT username FROM service_user WHERE id_user = ? ";

	   try (PreparedStatement pstmt = this.conn.prepareStatement(query)) {
		   pstmt.setInt(1, id);
		   ResultSet rs = pstmt.executeQuery();
		   if(rs.next()) {
			   result = rs.getString("username"); 
		   }
	   } catch(SQLException e) {
		   e.printStackTrace();
	   }
	   return result; 
	   
   }
   
   public void setPriority(int idMessage, String priority) {
	   	String query = "UPDATE service_message SET message_priority = ?::public.priority WHERE id_msg = ?"; 
	   	try (PreparedStatement pstmt = this.conn.prepareStatement(query)) {
	   		pstmt.setString(1, priority);
	   		pstmt.setInt(2, idMessage);
	   		pstmt.executeUpdate();
	   	
	   		
	   	} catch(SQLException e) {
	   		e.printStackTrace();
	   	}
   }
}

