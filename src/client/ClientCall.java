package client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.rmi.RemoteException;
import service.mail.MailServerStub;
import service.mail.MailServerStub.Message;
import service.mail.MailServerStub.GetMessages;
import service.mail.MailServerStub.ReadMessage;
import service.mail.MailServerStub.SendMessage;
import service.mail.MailServerStub.Delete;

public class ClientCall {
	private String username = "";

	public static void main(String[] args) {
		ClientCall client = new ClientCall();

		while (true) {
			client.start();
		}
	}

	public void start() {
		Form form = this.buildFormAuthentication();

		if (this.authenticate(form)) {
			System.out.println("Connected !\n");
			this.loop();
		} else {
			System.out.println("ERROR: Username or password is incorrect !\n");
		}
	}
	
	protected boolean authenticate(Form form) {
		ClientResource resource = new ClientResource("http://localhost:8082/authentication");
		String authenticationResponse = this.postRest(resource, form);

		return authenticationResponse.equals("User connected");
	}

	private Form buildFormAuthentication() {
		System.out.println("Welcome !\n"
				+ "Please enter your username and password to access your emails.\n");

		Scanner sc = new Scanner(System.in);

		System.out.print("Username : \t");
		this.username = sc.nextLine();
		System.out.println();
		System.out.print("Password : \t");
		String password = sc.nextLine();
		System.out.println();
		
		Form form = new Form();
		form.add("username", this.username);
		form.add("password", password);

		return form;
	}

	private String postRest(ClientResource resource, Form form) {
		String result = "";
		try {
			result = resource.post(form).getText();
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private void loop() {
	     int choice = -1;
	     Scanner sc = new Scanner(System.in);

	     while(choice != 5) {
	    	 System.out.println("Menu:\n"
	    		+ "1. Send an email\n"
	    		+ "2. Read emails\n"
	    		+ "3. Delete email\n"
	    		+ "4. Delete read emails\n"
	    		+ "5. Quit\n"
	    		+ "---\n"
	    		+ "6. Purge\n"
	    		+ "7. Acknowledge or delete\n"
	    		+ "8. Set late message (20h-00h) to high priority\n"
	    		+ "9. Exit\n");
	    	 
	    	 System.out.print("Choice: ");
	    	 choice = sc.nextInt();

	    	 try {
	    		 switch(choice) {
		    	 	case 1:
		    	 		this.writeEmail();
		    	 		break;
			        case 2:
			        	this.readEmails();
			        	break;
			        case 3:
			        	this.deleteEmail();
			        	break;
			        case 4:
			        	this.deleteReadEmails();
			        	break;
			        case 5:
			        	System.out.println("\nSee you !\n");
			        	break;
			        case 6:
			        	this.purge();
			        	break;
			        case 7:
			        	this.acknowledgeOrDelete();
			        	break;
			        case 8:
			        	this.lateMessageToHighPriority();
			        	break;
			        case 9:
			        	System.out.println("\nBye.\n");
			        	System.exit(0);
			        default:
			        	System.out.println("ERROR: Wrong input. Please try again.\n");
	    		 } 
	    	} catch (RemoteException e) {
	    		System.out.println("--- REMOTE ERROR ---");
	 			e.printStackTrace();
	 			System.out.println("--- END REMOTE ERROR ---");
	 		}
	    }
	}
	
	private void writeEmail() throws RemoteException {
		Scanner sc = new Scanner(System.in);

		System.out.println();
		System.out.print("To : \t");
		String recipient = sc.nextLine();
		System.out.println();
		System.out.print("Subject : \t");
		String subject = sc.nextLine();
		System.out.println();
		System.out.print("Content (press Enter at an empty line to finish): \n");
		String line = "line";
		String content = "";
        while (! line.isEmpty()) {
        	line = sc.nextLine();
        	if (! line.isEmpty()) {
        		content += line + "\n";
        	}
        }
		
	    String[] priorities = {"low priority", "important", "urgent"};
	    int choice = -1;
	    while (choice < 0 || choice > priorities.length - 1) {
	    	System.out.print("Priority (low = 0, important = 1, urgent = 2) : \t");
       	 	choice = sc.nextInt();
        }
		System.out.println();
	    
	    MailServerStub mss = new MailServerStub();
	    SendMessage sendMessage = new SendMessage(); 
	    sendMessage.setFrom(this.username);
	    sendMessage.setTo(recipient);
	    sendMessage.setSubject(subject);
	    sendMessage.setContent(content);
	    sendMessage.setPriority(priorities[choice]);
        
	    if (mss.sendMessage(sendMessage).get_return()) {
	    	System.out.println("Mail sent successfully !");
	    } else {
	    	System.out.println("ERROR: Couldn't send the email !");
	    }
		System.out.println();
	}
	
	private void readEmails() throws RemoteException {
		MailServerStub mss = new MailServerStub();
		GetMessages getMessages = new GetMessages(); 
		getMessages.setUser(this.username); 
        Message[] messages = mss.getMessages(getMessages).get_return();
        
        System.out.println();

        if (messages == null || messages.length == 0) {
        	System.out.println("Sorry, your mail box is empty !");
        } else {
        	Scanner sc = new Scanner(System.in);

            int choice = -1;
            while (choice != 0) {
            	this.printMessages(messages);

            	while (choice < 0 || choice > messages.length) {
                	System.out.print("Read mail number (0 to exit): ");
               	 	choice = sc.nextInt();
                }
            	
            	if (choice != 0) {
            		System.out.println();

            		Message m = messages[choice - 1];

            		if (m.get_new()) {
            			m.set_new(false);
            			ReadMessage readMessage = new ReadMessage(); 
            			readMessage.setIdMessage(m.getIdMessage()); 
            	        mss.readMessage(readMessage);
            		}

            		printMessage(m);
            		
            		System.out.println("Press any key to continue...");
            		
            		try {
    					System.in.read();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
            		
            		choice = -1;
            	}
            }
        }
        
        System.out.println();
	}
	
	private void deleteEmail() throws RemoteException {
		MailServerStub mss = new MailServerStub();
		GetMessages getMessages = new GetMessages(); 
		getMessages.setUser(this.username); 
        Message[] messages = mss.getMessages(getMessages).get_return();
        
        System.out.println();

        if (messages == null || messages.length == 0) {
        	System.out.println("Sorry, your mail box is empty !");
        } else {
	        Scanner sc = new Scanner(System.in);
	
	        int choice = -1;
	        this.printMessages(messages);
	
	    	while (choice < 0 || choice > messages.length) {
	        	System.out.print("Delete mail number (0 to exit): ");
	       	 	choice = sc.nextInt();
	       	 	System.out.println();
	        }
	    	
	    	if (choice != 0) {
    			Delete delete = new Delete(); 
    			delete.setIdMessage(messages[choice - 1].getIdMessage()); 
    	        mss.delete(delete).get_return();

	    		System.out.println("Mail deleted successfully.");
	    	}
        }

        System.out.println();
	}
	
	private void deleteReadEmails() throws RemoteException {
		MailServerStub mss = new MailServerStub();
		GetMessages getMessages = new GetMessages(); 
		getMessages.setUser(this.username); 
        ArrayList<Message> messages = new ArrayList<Message>(Arrays.asList(mss.getMessages(getMessages).get_return()));  

        List<Message> toDelete = messages.stream()
        		.filter(x -> ! x.get_new())
        		.collect(Collectors.toList());
        
        System.out.println();

        if (toDelete.size() == 0) {
        	System.out.println("Sorry, there no messages to delete !");
        } else {
        	for(Message m : toDelete) {
            	Delete del = new Delete(); 
	            del.setIdMessage(m.getIdMessage());
	            mss.delete(del).get_return();
            }
        	System.out.println("All read messages deleted.");
        }

        System.out.println();
	}
	
	private void printMessage(Message message) {
		System.out.println("From: " + message.getFrom());
		System.out.println("Priority: " + message.getPriority());
		System.out.println("Subject: " + message.getSubject());
		System.out.println("Content:\n" + message.getMessage());
		System.out.println();
	}
	
	public void printMessages(Message[] messages) {
		System.out.println(" num |          subject          |    from    |   priority   | read |         time        ");
        System.out.println("-----+---------------------------+------------+--------------+------|---------------------");
        
        for (int i = 0; i < messages.length; i++) {
        	Message m = messages[i];

        	for (int j = 0; j < 4 - Integer.toString(i + 1).length(); j++) {
        		System.out.print(" ");
            }

        	System.out.print(i + 1);
        	System.out.print(" | ");
        	this.cellPrinter(m.getSubject(), 25);
        	System.out.print(" | ");
        	this.cellPrinter(m.getFrom(), 10);
        	System.out.print(" | ");
        	this.cellPrinter(m.getPriority(), 12);
        	System.out.print(" | ");
        	if (m.get_new()) {
        		System.out.print("new!");
        	} else {
        		System.out.print("old.");
        	}
        	System.out.print(" | ");
        	System.out.println(m.getTime().substring(0, 19));
        }
        
        System.out.println();
	}
	
	private void cellPrinter(String str, int cellSize) {
    	if (str.length() > cellSize) {
    		System.out.print(str.substring(0, cellSize - 3) + "...");;
    	} else {
    		System.out.print(str);
    		for (int i = 0; i < cellSize - str.length(); i++) {
        		System.out.print(" ");;
            }
    	}
	}
	
	private void purge() {
		Form form = this.buildFormPurge();
		ClientResource resource = new ClientResource("http://localhost:8083/purge");
		String response = this.postRest(resource, form);

		if (response.equals("OK")) {
			System.out.println("Purged succesfully\n");
		} else {
			System.out.println(response);
			System.out.println("ERROR: Something unexpected happened\n");
		}
	}
	
	private Form buildFormPurge() {
		System.out.println("Remove message containing a word in the subject\n");

		Scanner sc = new Scanner(System.in);

		System.out.print("Word : \t");
		String toPurge = sc.nextLine();
		System.out.println();
		
		Form form = new Form();
		form.add("username", this.username);
		form.add("toPurge", toPurge);

		return form;
	}
	
	private void acknowledgeOrDelete() throws RemoteException {
		Form form = this.buildFormAcknowledgeOrDelete();
		ClientResource resource = new ClientResource("http://localhost:8083/acknowledgeOrDelete");
		int deleted = Integer.parseInt(this.postRest(resource, form));

		if (deleted >= 0) {
			System.out.println("Deleted " + deleted + " messages.\n");
		} else {
			System.out.println("ERROR: Something unexpected happened\n");
		}
	}
	
	private Form buildFormAcknowledgeOrDelete() {
		System.out.println("Acknowledge message from a user and delete the rest\n");

		Scanner sc = new Scanner(System.in);

		System.out.print("Acknowledge messages from : \t");
		String from = sc.nextLine();
		System.out.println();
		
		Form form = new Form();
		form.add("from", from);
		form.add("to", this.username);

		return form;
	}
	
	private void lateMessageToHighPriority() throws RemoteException {
		Form form = this.buildFormLateMessageToHighPriority();
		ClientResource resource = new ClientResource("http://localhost:8083/lateMessageToHighPriority");
		this.postRest(resource, form);
		System.out.println("Done !\n");
	}
	
	private Form buildFormLateMessageToHighPriority() {
		Form form = new Form();
		form.add("username", this.username);
		return form;
	}
}