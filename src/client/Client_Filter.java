package client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.restlet.data.Form;

import service.mail.MailServerStub;
import service.mail.MailServerStub.Delete;
import service.mail.MailServerStub.GetMessages;
import service.mail.MailServerStub.SendMessage;
import service.mail.MailServerStub.Message;

public class Client_Filter extends ClientCall {
	private static final String username = "user1"; 
	private static final String password = "password";
	private static final String specificAuthor = "user1";

	public static void main(String[] args) {
		Client_Filter c = new Client_Filter();

		Form form = new Form();
		form.add("username", username);
		form.add("password", password);
		
		if (c.authenticate(form)) {
			try {
				MailServerStub mss = new MailServerStub();
				GetMessages getMessages = new GetMessages(); 
				getMessages.setUser(username); 
				ArrayList<Message> messages = new ArrayList<Message>(Arrays.asList(mss.getMessages(getMessages).get_return()));
		        
		        System.out.println();
	
		        if (messages != null && messages.size() > 0) {
		            c.printMessages(mss.getMessages(getMessages).get_return());

		        	List<Message> toDelete = messages.stream()
		            		.filter(m -> ! m.getFrom().equals(specificAuthor))
		            		.collect(Collectors.toList());

	            	for(Message m : toDelete) {
	                	Delete del = new Delete(); 
	    	            del.setIdMessage(m.getIdMessage());
	    	            mss.delete(del).get_return();
	                }
	            	
	            	List<Message> toFeedback = messages.stream()
	            		.filter(m -> m.getFrom().equals(specificAuthor))
	            		.collect(Collectors.toList());
	            	
	            	for(Message m : toFeedback) {
	                	SendMessage sendMessage = new SendMessage(); 

	                	sendMessage.setFrom(username);
	            	    sendMessage.setTo(specificAuthor);
	            	    sendMessage.setSubject("Receipt confirmation");
	            	    sendMessage.setContent("Your message has been delivered.");
	            	    sendMessage.setPriority("low priority");
	                    
	            	    mss.sendMessage(sendMessage).get_return();
	                }
	            	
	            	c.printMessages(mss.getMessages(getMessages).get_return());
		        }
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
}
