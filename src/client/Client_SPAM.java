package client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.restlet.data.Form;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import java.rmi.RemoteException;
import service.mail.MailServerStub;
import service.mail.MailServerStub.Message;
import service.mail.MailServerStub.GetMessages;
import service.mail.MailServerStub.Delete;

public class Client_SPAM {
	private static final String username = "user1"; 
	private static final String password = "password"; 

	public static void main(String[] args) {
		ClientResource resource = new ClientResource("http://localhost:8082/authentication"); 
		
		Form form = buildFormAuthentication();
		
		String authenticationResponse = postAuthentication(resource, form);
		
		if (authenticationResponse.equals("User connected")) {
			System.out.println("Connected !\n");

			try {
				MailServerStub mss = new MailServerStub();
				GetMessages spam = new GetMessages(); 
	            spam.setUser(username); 
	            ArrayList<Message> response = new ArrayList<Message>(Arrays.asList(mss.getMessages(spam).get_return()));  

	            System.out.println(response.toString());

	            List<Message> toDelete = response.stream()
	            		.filter(x -> x.getSubject().contains("SPAM"))
	            		.collect(Collectors.toList());

	            System.out.println(toDelete.toString());

	            for(Message m : toDelete) {
	            	Delete del = new Delete(); 
		            del.setIdMessage(m.getIdMessage());
		            System.out.println(mss.delete(del).get_return());
	            }
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Username or password is incorrect\n");
		}
	}

	public static Form buildFormAuthentication() {
		
		Form form = new Form();
		form.add("username", username);
		form.add("password", password);
		return form;
	}

	public static String postAuthentication(ClientResource resource, Form form) {
		String result = "";
		try {
			result = resource.post(form).getText();
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}

