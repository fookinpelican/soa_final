package service.composite.rest;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.*;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

public class PurgerResource extends ServerResource {
	@Post
    public Representation acceptItem(Representation entity) {
        Form form = new Form(entity);
        String username = form.getFirstValue("username"); 
        String toPurge = form.getFirstValue("toPurge");

        Purger purger = new Purger();
        String result = purger.start(username, toPurge);
        
        System.out.println("RESULT: " + result);
        
        return new StringRepresentation(result, MediaType.TEXT_PLAIN);
    } 
}
