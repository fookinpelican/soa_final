package service.composite.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import util.FetchEmailDB;
import util.Message;

public class Purger {
	public String start(String username, String toPurge) {
		FetchEmailDB db = new FetchEmailDB();
		db.getDatabaseConnection();
		
		ArrayList<Message> response =  db.getAllMessagesForUser(username);

        List<Message> toDelete = response.stream()
    		.filter(x -> x.getSubject().contains(toPurge))
    		.collect(Collectors.toList());

        for(Message m : toDelete) {
        	db.removeMessage(m.getIdMessage());
        }
  
        return "OK";
	}
}
