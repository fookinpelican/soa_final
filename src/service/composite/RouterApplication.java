package service.composite;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class RouterApplication extends Application{
	/**
	 * Creates a root Restlet that will receive all incoming calls.
	 */
	@Override
	public synchronized Restlet createInboundRoot() {
		Router router = new Router(getContext());
		router.attach("/purge", PurgerResource.class);
		router.attach("/acknowledgeOrDelete", AcknowledgeResource.class);
		router.attach("/lateMessageToHighPriority", LateMessageResource.class);

		return router;
	}
}