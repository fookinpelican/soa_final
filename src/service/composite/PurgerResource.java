package service.composite;

import java.io.IOException;

import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Protocol;
import org.restlet.representation.*;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class PurgerResource extends ServerResource {
	@Post
    public Representation acceptItem(Representation entity) {
        Form form = new Form(entity);

        String result = "";
        try {
        	Client client = new Client(new Context(), Protocol.HTTP);
    		ClientResource clientResource = new ClientResource("http://localhost:8084/purge");
    		clientResource.setNext(client);
			result = clientResource.post(form).getText();
		} catch (ResourceException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new StringRepresentation(result, MediaType.TEXT_PLAIN);
    } 
}
