package service.composite;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import service.composite.soap.MailCompositeServerStub;
import service.composite.soap.MailCompositeServerStub.LateMessageToHighPriority;

public class LateMessageResource extends ServerResource {
	@Post
    public Representation acceptItem(Representation entity) {
		Form form = new Form(entity);
		String username = form.getFirstValue("username"); 

        String result = "ERROR";
		MailCompositeServerStub mss;
		try {
			mss = new MailCompositeServerStub();
			LateMessageToHighPriority lmth = new LateMessageToHighPriority();
			lmth.setUsername(username);
			mss.lateMessageToHighPriority(lmth);
			result = "OK";
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new StringRepresentation(result, MediaType.TEXT_PLAIN);
    } 
}
