package service.composite.soap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import util.FetchEmailDB;
import util.Message;

public class MailCompositeServer {
	public int acknowledgeOrDelete(String from, String to) {
		FetchEmailDB db = new FetchEmailDB(); 
		db.getDatabaseConnection();
		
		ArrayList<Message> toAcknowledge =  db.getAllMessagesForUser(to);
		
		ArrayList<Message> toDelete = new ArrayList<Message>();

		Iterator<Message> i = toAcknowledge.iterator();
		while (i.hasNext()) {
			Message m = i.next();
			if (! m.getFrom().equals(from)) {
				toDelete.add(m);
				i.remove();
			}
		}
        
        for (Message m : toAcknowledge) {
    	    String subject = "Receipt confirmation";
    	    String content = "Your message has been delivered.";
    	    String priority = "low priority";
    	    db.sendMessage(to, m.getFrom(), subject, content, priority);
        }

        for(Message m : toDelete) {
        	db.removeMessage(m.getIdMessage());
        }
        
        return toDelete.size();
	}

	public void lateMessageToHighPriority(String username) {
		FetchEmailDB db = new FetchEmailDB(); 
		db.getDatabaseConnection();

		ArrayList<Message> response =  db.getAllMessagesForUser(username);

		for (Message m : response) {
			int hour = Integer.parseInt(m.getTime().split(" ")[1].split(":")[0]);
			if (hour >= 20 || hour == 0) {
				db.setPriority(m.getIdMessage(), "important");
			}
		}
	}
}
