package service.composite;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import service.composite.soap.MailCompositeServerStub;
import service.composite.soap.MailCompositeServerStub.AcknowledgeOrDelete;

public class AcknowledgeResource extends ServerResource {
	@Post
    public Representation acceptItem(Representation entity) {
		Form form = new Form(entity);
		String from = form.getFirstValue("from"); 
        String to = form.getFirstValue("to");

        int deleted = -1;
		MailCompositeServerStub mss;
		try {
			mss = new MailCompositeServerStub();
			AcknowledgeOrDelete aod = new AcknowledgeOrDelete();
			aod.setFrom(from);
			aod.setTo(to);
			deleted = mss.acknowledgeOrDelete(aod).get_return();
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new StringRepresentation(String.valueOf(deleted), MediaType.TEXT_PLAIN);
    } 
}
