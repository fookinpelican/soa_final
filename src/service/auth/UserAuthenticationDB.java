package service.auth;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserAuthenticationDB {

	private final String url = "jdbc:postgresql://localhost/WS";
    private final String user = "postgres";
	
    public String validateAuthentication(String user, String password) {
    	Connection conn = getDatabaseConnection();
    	if (!isUserKnown(user, conn)) {
    		return "Unknown user"; 
    	}
    	else if (!isPasswordValid(user, password, conn)) {
    		return "wrong password"; 
    	}
    	return "User connected"; 
    }
    
    private Connection getDatabaseConnection() {
        Connection conn = null; 
        try {
            conn = DriverManager.getConnection(url, user, "");
            System.out.println("Successfully conencted to the database!");
        } catch (SQLException e) {
            System.out.print(e.getMessage());
        }
 
        return conn; 
    }

    private boolean isPasswordValid(String user, String password, Connection conn) {
    	boolean result = false;
    	String query = "SELECT username, password FROM service_user WHERE username = ?"; 
    	try(PreparedStatement pstmt = conn.prepareStatement(query)) {
        	pstmt.setString(1, user);
            ResultSet rs = pstmt.executeQuery(); 
            if(rs.next()) {
            	result = rs.getString("username").equals(user) &&
            			rs.getString("password").equals(password); 
            }
        }catch(SQLException e) {
            e.printStackTrace();
        }
    	return result; 
    }

    private boolean isUserKnown(String user, Connection conn) {
    	boolean result = false; 
        String query = "SELECT * FROM service_user WHERE username = ?"; 
        try(PreparedStatement pstmt = conn.prepareStatement(query)) {
        	pstmt.setString(1, user);
            ResultSet rs = pstmt.executeQuery(); 
            result = rs.next(); 
        }catch(SQLException e) {
            e.printStackTrace();
        }
        return result; 
     }
}
