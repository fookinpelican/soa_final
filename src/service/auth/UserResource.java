package service.auth;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.representation.*;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
 
public class UserResource extends ServerResource {  	


	@Post
	    public Representation acceptItem(Representation entity) {  

			UserAuthenticationDB db = new UserAuthenticationDB(); 
			// Parse the given representation and retrieve data
	        Form form = new Form(entity);  
	        String username = form.getFirstValue("username");  
	        String password = form.getFirstValue("password");  
	        
	        String resultDB = db.validateAuthentication(username, password); 
	        
	        return new StringRepresentation(resultDB, MediaType.TEXT_PLAIN);
//	 
//	        if(uid.equals("123")){ // Assume that user id 123 is existed
//	        result = new StringRepresentation("User whose uid="+ uid +" is updated",  
//	            MediaType.TEXT_PLAIN);
//	        } 
//	        else { // otherwise add user  
//	        result = new StringRepresentation("User " + uname + " is added",  
//	            MediaType.TEXT_PLAIN);
//	        }  
	 
	    } 
	
	
	
}  