package service.mail;

import java.util.ArrayList;

import util.FetchEmailDB;
import util.Message;

public class MailServer {
	public static void main(String[] args) {
		MailServer ms = new MailServer();
//		System.out.println(ms.removeSpam("user1"));
		System.out.println(ms.getMessages("user3").toString());
	}

	// localhost:8080/WS_HelloWorld/services/MailServer/ping?string=???
	public String ping(String string) {
		return "ping: " + string;
	}

	public ArrayList<Message> getMessages(String user) {
		FetchEmailDB db = new FetchEmailDB(); 
		db.getDatabaseConnection();
		return db.getAllMessagesForUser(user); 
	}

	public String delete(int idMessage) {
		FetchEmailDB db = new FetchEmailDB(); 
		db.getDatabaseConnection();

		if(db.removeMessage(idMessage)) {
			return "message deleted"; 
		}
		return "an error occured"; 
	}
	
	public void readMessage(int idMessage) {
		FetchEmailDB db = new FetchEmailDB(); 
		db.getDatabaseConnection();
		db.makeMeassageSeen(idMessage);
	}
	
	public boolean sendMessage(String from, String to, String subject, String content, String priority) {
		FetchEmailDB db = new FetchEmailDB(); 
		db.getDatabaseConnection();
		return db.sendMessage(from, to, subject, content, priority);
	}
}