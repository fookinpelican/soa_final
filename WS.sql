--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: priority; Type: TYPE; Schema: public; Owner: gawlk
--

CREATE TYPE public.priority AS ENUM (
    'urgent',
    'important',
    'low priority'
);


ALTER TYPE public.priority OWNER TO gawlk;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: service_message; Type: TABLE; Schema: public; Owner: gawlk
--

CREATE TABLE public.service_message (
    id_msg integer NOT NULL,
    author integer,
    recipient integer,
    subject character varying,
    content character varying NOT NULL,
    seen boolean DEFAULT false NOT NULL,
    message_priority public.priority DEFAULT 'low priority'::public.priority NOT NULL,
    time timestamp without time zone DEFAULT now()
);


ALTER TABLE public.service_message OWNER TO gawlk;

--
-- Name: service_message_id_msg_seq; Type: SEQUENCE; Schema: public; Owner: gawlk
--

CREATE SEQUENCE public.service_message_id_msg_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_message_id_msg_seq OWNER TO gawlk;

--
-- Name: service_message_id_msg_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gawlk
--

ALTER SEQUENCE public.service_message_id_msg_seq OWNED BY public.service_message.id_msg;


--
-- Name: service_user; Type: TABLE; Schema: public; Owner: gawlk
--

CREATE TABLE public.service_user (
    id_user integer NOT NULL,
    username character varying NOT NULL,
    password text NOT NULL
);


ALTER TABLE public.service_user OWNER TO gawlk;

--
-- Name: service_user_id_user_seq; Type: SEQUENCE; Schema: public; Owner: gawlk
--

CREATE SEQUENCE public.service_user_id_user_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_user_id_user_seq OWNER TO gawlk;

--
-- Name: service_user_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gawlk
--

ALTER SEQUENCE public.service_user_id_user_seq OWNED BY public.service_user.id_user;


--
-- Name: service_message id_msg; Type: DEFAULT; Schema: public; Owner: gawlk
--

ALTER TABLE ONLY public.service_message ALTER COLUMN id_msg SET DEFAULT nextval('public.service_message_id_msg_seq'::regclass);


--
-- Name: service_user id_user; Type: DEFAULT; Schema: public; Owner: gawlk
--

ALTER TABLE ONLY public.service_user ALTER COLUMN id_user SET DEFAULT nextval('public.service_user_id_user_seq'::regclass);


--
-- Data for Name: service_message; Type: TABLE DATA; Schema: public; Owner: gawlk
--

COPY public.service_message (id_msg, author, recipient, subject, content, seen, message_priority) FROM stdin;
1	3	2	Video chat	lien vers video	f	urgent
2	4	2	Signature contrat	Veuillez signer le contrat joint	t	urgent
3	2	3	Dejeuner	Je vous invite à dejeuner	f	low priority
4	2	4	Migration	Migration ce weekend, pensez à eteindre vos machines	f	important
\.


--
-- Data for Name: service_user; Type: TABLE DATA; Schema: public; Owner: gawlk
--

COPY public.service_user (id_user, username, password) FROM stdin;
2	user1	password
3	user2	pass
4	user3	password
\.


--
-- Name: service_message_id_msg_seq; Type: SEQUENCE SET; Schema: public; Owner: gawlk
--

SELECT pg_catalog.setval('public.service_message_id_msg_seq', 4, true);


--
-- Name: service_user_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: gawlk
--

SELECT pg_catalog.setval('public.service_user_id_user_seq', 4, true);


--
-- Name: service_user service_user_pkey; Type: CONSTRAINT; Schema: public; Owner: gawlk
--

ALTER TABLE ONLY public.service_user
    ADD CONSTRAINT service_user_pkey PRIMARY KEY (id_user);


--
-- Name: service_user unique_username_cc; Type: CONSTRAINT; Schema: public; Owner: gawlk
--

ALTER TABLE ONLY public.service_user
    ADD CONSTRAINT unique_username_cc UNIQUE (username);


--
-- Name: service_message author_fk; Type: FK CONSTRAINT; Schema: public; Owner: gawlk
--

ALTER TABLE ONLY public.service_message
    ADD CONSTRAINT author_fk FOREIGN KEY (author) REFERENCES public.service_user(id_user);


--
-- Name: service_message recipient_fk; Type: FK CONSTRAINT; Schema: public; Owner: gawlk
--

ALTER TABLE ONLY public.service_message
    ADD CONSTRAINT recipient_fk FOREIGN KEY (recipient) REFERENCES public.service_user(id_user);


--
-- PostgreSQL database dump complete
--

